Hej.

Ten szybki i prosty projekt ma na celu opisanie sposobu instalacji Dashy na Debian GNU Linux + Docker.

Żródła z których warto korzystać, podczas instalacji oraz konfiguracji:

- [Debian GNU Linux](https://www.debian.org)
- [Docker](https://docs.docker.com)
- [Dashy](https://dashy.to)

Aby zacząć, przejdź do sekcji [Wiki](https://gitlab.com/r-gorajski/dashy-docker/-/wikis/home), czytaj, instaluj i baw się dobrze!!!
